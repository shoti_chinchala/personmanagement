﻿using Persons.Core.Commands.CreatePerson;
using System;
using System.Collections.Generic;
using System.Text;

namespace Persons.Core.Queries.GetPerson
{
    public class PersonDTO
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string PersonalNumber { get; set; }
        public DateTime BirthDate { get; set; }
        public int GenderId { get; set; }
        public string Gender { get; set; }
        public int CityId { get; set; }
        public string City { get; set; }
        public string ImagePath { get; set; }
        public IEnumerable<MobileDTO> Mobiles { get; set; }
        public IEnumerable<ConnectionDTO> Connections { get; set; }
    }

    public class MobileDTO
    {
        public int Id { get; set; }
        public int MobileTypeId { get; set; }
        public string MobileType { get; set; }
        public string Number { get; set; }
    }

    public class ConnectionDTO
    {
        public int ConnectionTypeId { get; set; }
        public string ConnectionType { get; set; }
        public int ConnectedPersonId { get; set; }
        public string ConnectedPersonFirstName { get; set; }
        public string ConnectedPersonLastName { get; set; }
        public string ConnectedPersonPersonalNumber { get; set; }
    }
}
