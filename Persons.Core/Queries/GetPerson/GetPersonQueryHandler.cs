﻿using AutoMapper;
using MediatR;
using Microsoft.AspNetCore.Server.IIS;
using Microsoft.Extensions.Localization;
using Persons.Core.Commands.AddConnectedPerson;
using Persons.Core.Common.Exceptions;
using Persons.Core.Common.Resources;
using Persons.Core.Repositories;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Persons.Core.Queries.GetPerson
{
    public class GetPersonQueryHandler : IRequestHandler<GetPersonQuery, PersonDTO>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        private readonly IStringLocalizer<ValidationResource> _localizer;

        public GetPersonQueryHandler(IUnitOfWork unitOfWork, IMapper mapper, IStringLocalizer<ValidationResource> localizer)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
            _localizer = localizer;
        }

        public async Task<PersonDTO> Handle(GetPersonQuery request, CancellationToken cancellationToken)
        {
            var person = await _unitOfWork.PersonRepository.GetAsync(request.Id);

            if (person is null)
                throw new NotFoundException(_localizer["EntityNotFound", "Person", request.Id]);

            return _mapper.Map<PersonDTO>(person);
        }
    }
}