﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Persons.Core.Queries.GetPerson
{
    public class GetPersonQuery : IRequest<PersonDTO>
    {
        public int Id { get; set; }
    }
}
