﻿using AutoMapper;
using MediatR;
using Persons.Core.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Persons.Core.Queries.ConnectionsReport
{
    public class ConnectionsReportQueryHandler : IRequestHandler<ConnectionsReportQuery, List<ConnectionsReporDTO>>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public ConnectionsReportQueryHandler(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public async Task<List<ConnectionsReporDTO>> Handle(ConnectionsReportQuery request, CancellationToken cancellationToken)
        {
            var report = await _unitOfWork.PersonRepository.ConnectionsReport();

            return report.ToList();
        }
    }
}
