﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Persons.Core.Queries.ConnectionsReport
{
    public class ConnectionsReporDTO
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string PersonalNumber { get; set; }
        public int ConnectionTypeId { get; set; }
        public int Connections { get; set; }
    }
}
