﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace Persons.Core.Queries.ConnectionsReport
{
    public class ConnectionsReportQuery :IRequest<List<ConnectionsReporDTO>>
    {
    }
}
