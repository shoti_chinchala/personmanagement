﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Persons.Core.Queries.GetPersonList
{
    public class PersonListDTO
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string PersonalNumber { get; set; }
    }
}
