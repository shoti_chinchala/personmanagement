﻿using FluentValidation;
using Microsoft.Extensions.Localization;
using Persons.Core.Common.Resources;
using System;
using System.Collections.Generic;
using System.Text;

namespace Persons.Core.Queries.GetPersonList
{
    class GetPersonListQueryValidator : AbstractValidator<GetPersonListQuery>
    {
        public GetPersonListQueryValidator(IStringLocalizer<ValidationResource> localizer)
        {
            RuleFor(x => x.PerPage)
                  .GreaterThanOrEqualTo(10)
                  .LessThanOrEqualTo(50)
                  .WithMessage(localizer["PerPageNotCorrect"]);
        }
    }
}
