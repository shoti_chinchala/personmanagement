﻿using AutoMapper;
using MediatR;
using Microsoft.AspNetCore.Server.IIS;
using Persons.Core.Commands.AddConnectedPerson;
using Persons.Core.Common.Models;
using Persons.Core.Domain;
using Persons.Core.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Persons.Core.Queries.GetPersonList
{
    public class GetPersonListQueryHandler : IRequestHandler<GetPersonListQuery, List<PersonListDTO>>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public GetPersonListQueryHandler(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public async Task<List<PersonListDTO>> Handle(GetPersonListQuery request, CancellationToken cancellationToken)
        {
            var filter = _mapper.Map<PersonFilter>(request);
            var personList = await _unitOfWork.PersonRepository.ListAsync(filter);

            return _mapper.Map<List<PersonListDTO>>(personList.ToList());
        }
    }
}