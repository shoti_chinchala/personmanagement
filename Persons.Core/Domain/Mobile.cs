﻿using Persons.Core.Domain.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace Persons.Core.Domain
{
    public class Mobile
    {
        public int Id { get; set; }
        public MobileTypeEnum MobileTypeId { get; set; }
        public MobileType MobileType { get; set; }
        public string Number { get; set; }
    }
}
