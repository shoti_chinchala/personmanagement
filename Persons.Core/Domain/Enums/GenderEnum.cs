﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Persons.Core.Domain.Enums
{
    public enum GenderEnum
    {
        [Display(Name = "ქალი")]
        Male = 1,

        [Display(Name = "კაცი")]
        Female = 2
    }
}
