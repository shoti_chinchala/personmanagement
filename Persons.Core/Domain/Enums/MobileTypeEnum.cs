﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Persons.Core.Domain.Enums
{
    public enum MobileTypeEnum
    {
        [Display(Name = "მობილური")]
        Mobile = 1,

        [Display(Name = "ოფისის")]
        Office = 2,

        [Display(Name = "სახლის")]
        Home = 3
    }
}
