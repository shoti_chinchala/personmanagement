﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Persons.Core.Domain.Enums
{
    public enum ConnectionTypeEnum
    {
        [Display(Name = "კოლეგა")]
        Ccolleague = 1,

        [Display(Name = "ნაცნობი")]
        Acquaintance = 2,

        [Display(Name = "ნათესავი")]
        Relative = 3,

        [Display(Name = "სხვა")]
        Other = 4,
    }
}
