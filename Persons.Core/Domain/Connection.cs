﻿using Persons.Core.Domain.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace Persons.Core.Domain
{
    public class Connection
    {
        public ConnectionTypeEnum ConnectionTypeId { get; set; }
        public ConnectionType ConnectionType { get; set; }
        public int PersonId { get; set; }
        public Person Person { get; set; }
        public int ConnectedPersonId { get; set; }
        public Person ConnectedPerson { get; set; }
    }
}
