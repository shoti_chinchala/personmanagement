﻿using Persons.Core.Domain.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace Persons.Core.Domain
{
    public class ConnectionType
    {
        public ConnectionTypeEnum Id { get; set; }
        public string Name { get; set; }
    }
}
