﻿using Persons.Core.Domain.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace Persons.Core.Domain
{
    public class MobileType
    {
        public MobileTypeEnum Id { get; set; }
        public string Name { get; set; }
    }
}
