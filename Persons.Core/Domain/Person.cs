﻿using Persons.Core.Domain.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace Persons.Core.Domain
{
    public class Person : IIsDeleted
    {
        public Person()
        {
            Mobiles = new List<Mobile>();
            Connections = new List<Connection>();
        }
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string PersonalNumber { get; set; }
        public DateTime BirthDate { get; set; }
        public GenderEnum GenderId { get; set; }
        public Gender Gender { get; set; }
        public int CityId { get; set; }
        public City City { get; set; }
        public string ImagePath { get; set; }
        public IList<Mobile> Mobiles { get; set; }
        public IList<Connection> Connections { get; set; }
        public bool IsDeleted { get; set; }
    }
}
