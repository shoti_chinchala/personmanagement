﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Persons.Core.Domain
{
    public interface IIsDeleted
    {
        bool IsDeleted { get; set; }
    }
}
