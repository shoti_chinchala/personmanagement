﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Persons.Core.Repositories
{
    public interface IUnitOfWork : IDisposable
    {
        IPersonRepository PersonRepository { get; }
        Task<int> CompleteAsync();
    }
}
