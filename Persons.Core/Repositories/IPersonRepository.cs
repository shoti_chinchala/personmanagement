﻿using Persons.Core.Common.Models;
using Persons.Core.Domain;
using Persons.Core.Queries.ConnectionsReport;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Persons.Core.Repositories
{
    public interface IPersonRepository : IRepository<Person>
    {
        Task<IEnumerable<Person>> ListAsync(PersonFilter filter);
        public Task<IEnumerable<ConnectionsReporDTO>> ConnectionsReport();
    }
}
