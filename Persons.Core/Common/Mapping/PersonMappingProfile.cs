﻿using AutoMapper;
using Persons.Core.Commands.AddConnectedPerson;
using Persons.Core.Commands.CreatePerson;
using Persons.Core.Commands.UpdatePerson;
using Persons.Core.Common.Models;
using Persons.Core.Domain;
using Persons.Core.Queries.GetPerson;
using Persons.Core.Queries.GetPersonList;
using System;
using System.Collections.Generic;
using System.Text;

namespace Persons.Core.Common.Mapping
{
    public class PersonMappingProfile : Profile
    {
        public PersonMappingProfile()
        {
            CreateMap<CreatePersonCommand, Person>();
            CreateMap<UpdatePersonCommand, Person>();
            CreateMap<CreateMobileDTO, Mobile>();
            CreateMap<UpdateMobileDTO, Mobile>();
            CreateMap<GetPersonListQuery, PersonFilter>();
            CreateMap<Person, PersonListDTO>();
            CreateMap<AddConnectedPersonCommand, Connection>();

            CreateMap<Person, PersonDTO>()
                .ForMember(x => x.Gender, a => a.MapFrom(s => s.Gender.Name))
                .ForMember(x => x.City, a => a.MapFrom(s => s.City.Name));

            CreateMap<Mobile, MobileDTO>()
                .ForMember(x => x.MobileType, a => a.MapFrom(s => s.MobileType.Name));

            CreateMap<Connection,ConnectionDTO>()
                .ForMember(x => x.ConnectionType, a => a.MapFrom(s => s.ConnectionType.Name))
                .ForMember(x => x.ConnectedPersonFirstName, a => a.MapFrom(s => s.ConnectedPerson.FirstName))
                .ForMember(x => x.ConnectedPersonLastName, a => a.MapFrom(s => s.ConnectedPerson.LastName))
                .ForMember(x => x.ConnectedPersonPersonalNumber, a => a.MapFrom(s => s.ConnectedPerson.PersonalNumber));
        }
    }
}
