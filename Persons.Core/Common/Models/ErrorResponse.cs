﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Persons.Core.Common.Models
{
    public class ErrorResponse
    {
        public string Message { get; set; }
        public List<string> ValidationErrors { get; set; }
    }
}
