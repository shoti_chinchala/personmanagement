﻿using Persons.Core.Domain.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace Persons.Core.Common.Models
{
    public class PersonFilter
    {
        public string? FirstName { get; set; }
        public string? LastName { get; set; }
        public string? PersonalNumber { get; set; }
        public DateTime? BirthDate { get; set; }
        public GenderEnum? GenderId { get; set; }
        public int? CityId { get; set; }
        public string? MobileNumber { get; set; }
        public int? Page { get; set; }
        public int PerPage { get; set; }
    }
}
