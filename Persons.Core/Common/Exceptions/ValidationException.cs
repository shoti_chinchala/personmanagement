﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Persons.Core.Common.Exceptions
{
    public class ValidationException : Exception
    {
        public List<string> Errors { get; set; }
        public ValidationException(List<string> errors) : base("Validation Error")
        {
            Errors = errors;
        }

        public ValidationException(string error) : base("Validation Error")
        {
            Errors = new List<string>() { error };
        }
    }
}
