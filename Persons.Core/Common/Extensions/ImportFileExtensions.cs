﻿using AutoMapper.Configuration;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace Persons.Core.Common.Extensions
{
    public static class ImportFileExtensions
    {
        public async static Task<string> Save(this IFormFile file, string path, string root, string existingPath = null)
        {
            var filePath = existingPath;
            if (string.IsNullOrEmpty(filePath))
            {
                string extension = Path.GetExtension(file.FileName);
                filePath = Path.Combine(path, Guid.NewGuid().ToString()) + extension;
            }
            var fullPath = Path.Combine(root, filePath);
            using (var fileStream = new FileStream(fullPath, FileMode.Create))
            {
                await file.CopyToAsync(fileStream);
            }
            return filePath;
        }
    }
}
