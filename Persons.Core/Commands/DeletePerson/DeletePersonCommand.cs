﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace Persons.Core.Commands.DeletePerson
{
    public class DeletePersonCommand : IRequest
    {
        public int Id{ get; set; }
    }
}
