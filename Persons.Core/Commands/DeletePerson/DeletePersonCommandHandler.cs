﻿using AutoMapper;
using MediatR;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.Localization;
using Persons.Core.Common.Exceptions;
using Persons.Core.Common.Resources;
using Persons.Core.Repositories;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Persons.Core.Commands.DeletePerson
{
    public class DeletePersonCommandHandler : IRequestHandler<DeletePersonCommand>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IStringLocalizer<ValidationResource> _localizer;

        public DeletePersonCommandHandler(IUnitOfWork unitOfWork, IStringLocalizer<ValidationResource> localizer)
        {
            _unitOfWork = unitOfWork;
            _localizer = localizer;
        }

        public async Task<Unit> Handle(DeletePersonCommand request, CancellationToken cancellationToken)
        {
            var person = await _unitOfWork.PersonRepository.GetAsync(request.Id);

            if (person is null)
                throw new NotFoundException(_localizer["EntityNotFound", "Person", request.Id]);

            _unitOfWork.PersonRepository.Remove(person);

            await _unitOfWork.CompleteAsync();

            return Unit.Value;
        }
    }

}
