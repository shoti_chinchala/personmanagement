﻿using FluentValidation;
using Microsoft.Extensions.Localization;
using Persons.Core.Common.Resources;
using System;
using System.Collections.Generic;
using System.Security.Cryptography.X509Certificates;
using System.Text;

namespace Persons.Core.Commands.DeletePerson
{
    public class DeletePersonCommandValidator : AbstractValidator<DeletePersonCommand>
    {
        public DeletePersonCommandValidator(IStringLocalizer<ValidationResource> localizer)
        {
            RuleFor(x => x.Id)
                .GreaterThan(0)
                .WithMessage(localizer["IdShouldBeMoreThanZero"]);
        }
    }
}
