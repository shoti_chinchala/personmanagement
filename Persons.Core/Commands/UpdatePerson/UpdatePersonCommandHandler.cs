﻿using AutoMapper;
using MediatR;
using Microsoft.Extensions.Localization;
using Persons.Core.Common.Resources;
using Persons.Core.Domain;
using Persons.Core.Repositories;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Persons.Core.Commands.UpdatePerson
{
    public class UpdatePersonCommandHandler : IRequestHandler<UpdatePersonCommand>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public UpdatePersonCommandHandler(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public async Task<Unit> Handle(UpdatePersonCommand request, CancellationToken cancellationToken)
        {
            var person = _mapper.Map<UpdatePersonCommand, Person>(request);

            if (person is null)
                throw new ArgumentNullException("Person");

            await _unitOfWork.PersonRepository.AddAsync(person);

            await _unitOfWork.CompleteAsync();

            return Unit.Value;
        }
    }
}
