﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace Persons.Core.Commands.UpdatePerson
{
    public class UpdatePersonCommand : IRequest
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string PersonalNumber { get; set; }
        public DateTime BirthDate { get; set; }
        public int GenderId { get; set; }
        public int CityId { get; set; }
        public string ImageBase64 { get; set; }
        public IEnumerable<UpdateMobileDTO> Mobiles { get; set; }
    }
    public class UpdateMobileDTO
    {
        public int MobileTypeId { get; set; }
        public string Number { get; set; }
    }
}
