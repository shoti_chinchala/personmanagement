﻿using MediatR;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Text;

namespace Persons.Core.Commands.UploadImage
{
    public class UploadImageCommand : IRequest
    {
        public int Id { get; set; }
        public IFormFile File { get; set; }
    }
}
