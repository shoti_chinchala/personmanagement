﻿using MediatR;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Persons.Core.Common.Exceptions;
using Persons.Core.Common.Extensions;
using Persons.Core.Repositories;
using System.IO;
using System.Threading;
using System.Threading.Tasks;

namespace Persons.Core.Commands.UploadImage
{
    public class UploadImageCommandHandler : IRequestHandler<UploadImageCommand>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IConfiguration _config;
        private readonly IWebHostEnvironment _env;

        public UploadImageCommandHandler(IUnitOfWork unitOfWork, IConfiguration config, IWebHostEnvironment env)
        {
            _unitOfWork = unitOfWork;
            _config = config;
            _env = env;
        }

        public async Task<Unit> Handle(UploadImageCommand request, CancellationToken cancellationToken)
        {
            var person = await _unitOfWork.PersonRepository.GetAsync(request.Id);
            if (person is null)
                throw new NotFoundException("Person");

            var filePath = await request.File.Save(_config.GetSection("UploadSettings:UploadPath").Value, _env.WebRootPath, person.ImagePath);
            person.ImagePath = filePath;

            await _unitOfWork.CompleteAsync();
            return Unit.Value;
        }
    }
}
