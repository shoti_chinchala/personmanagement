﻿using FluentValidation;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Localization;
using Persons.Core.Common.Resources;
using System.Collections.Generic;
using System.Linq;

namespace Persons.Core.Commands.UploadImage
{
    public class UploadImageCommandValidator : AbstractValidator<UploadImageCommand>
    {
        public UploadImageCommandValidator(IStringLocalizer<ValidationResource> localizer, IConfiguration config)
        {
            RuleFor(x => x.Id)
                .GreaterThan(0)
                .WithMessage(localizer["IdShouldBeMoreThanZero"]);

            RuleFor(x => x.File)
                .SetValidator(new FileValidator(localizer, config));
        }


    }

    public class FileValidator : AbstractValidator<IFormFile>
    {
        public FileValidator(IStringLocalizer<ValidationResource> localizer, IConfiguration config)
        {
            var maxSize = config.GetSection("UploadSettings:MaxSize").Get<int>();
            var formats = config.GetSection("UploadSettings:SupportedFormats").Get<List<string>>();
            RuleFor(x => x.Length).NotNull().LessThanOrEqualTo(maxSize)
                .WithMessage(localizer["FileGreaterThan", maxSize]);

            RuleFor(x => x.ContentType).NotNull().Must(x => formats.Contains(x))
                .WithMessage(localizer["FileNotInAllowedFormats", string.Join(";", formats)]);
        }
    }
}
