﻿using FluentValidation;
using Microsoft.Extensions.Localization;
using Persons.Core.Common.Resources;
using Persons.Core.Domain;
using Persons.Core.Domain.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace Persons.Core.Commands.AddConnectedPerson
{
    public class AddConnectedPersonCommandValidator : AbstractValidator<AddConnectedPersonCommand>
    {
        public AddConnectedPersonCommandValidator(IStringLocalizer<ValidationResource> localizer)
        {
            RuleFor(x => x.PersonId)
                .GreaterThan(0)
                .WithMessage(localizer["IdShouldBeMoreThanZero"]);

            RuleFor(x => x.ConnectedPersonId)
                .GreaterThan(0)
                .WithMessage(localizer["IdShouldBeMoreThanZero"]);

            RuleFor(x => x.ConnectionTypeId)
                .Must(x => Enum.IsDefined(typeof(ConnectionTypeEnum), x))
                .WithMessage(localizer["IncorrectConnectionType"]);
        }
    }
}
