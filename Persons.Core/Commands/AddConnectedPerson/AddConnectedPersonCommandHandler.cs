﻿using AutoMapper;
using MediatR;
using Microsoft.Extensions.Localization;
using Persons.Core.Domain;
using Persons.Core.Common.Exceptions;
using Persons.Core.Repositories;
using Persons.Core.Common.Resources;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Persons.Core.Commands.AddConnectedPerson
{
    public class AddConnectedPersonCommandHandler : IRequestHandler<AddConnectedPersonCommand>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IStringLocalizer<ValidationResource> _localizer;
        private readonly IMapper _mapper;

        public AddConnectedPersonCommandHandler(IUnitOfWork unitOfWork, IStringLocalizer<ValidationResource> localizer, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _localizer = localizer;
            _mapper = mapper;
        }

        public async Task<Unit> Handle(AddConnectedPersonCommand request, CancellationToken cancellationToken)
        {
            var person = await _unitOfWork.PersonRepository.GetAsync(request.PersonId);
            if (person is null)
                throw new NotFoundException(_localizer["EntityNotFound", "Person", request.PersonId]);

            var connetedPerson = await _unitOfWork.PersonRepository.GetAsync(request.ConnectedPersonId);
            if (connetedPerson is null)
                throw new NotFoundException(_localizer["EntityNotFound", "ConnetedPerson", request.ConnectedPersonId]);

            if (person.Connections.Any(x => x.ConnectedPersonId == request.ConnectedPersonId))
                throw new ValidationException(_localizer["PersonsAlreadyconnected"]);


            var connection = _mapper.Map<Connection>(request);
            var a = person.Connections.ToList();
            a.Add(connection);
            person.Connections = a;
            await _unitOfWork.CompleteAsync();
            return Unit.Value;
        }
    }
}