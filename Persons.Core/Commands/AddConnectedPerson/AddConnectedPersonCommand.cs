﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace Persons.Core.Commands.AddConnectedPerson
{
    public class AddConnectedPersonCommand : IRequest
    {
        public int ConnectionTypeId { get; set; }
        public int PersonId { get; set; }
        public int ConnectedPersonId { get; set; }
    }
}
