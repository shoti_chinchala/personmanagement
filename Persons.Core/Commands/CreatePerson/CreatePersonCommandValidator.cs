﻿using FluentValidation;
using Microsoft.Extensions.Localization;
using Persons.Core.Common.Resources;
using Persons.Core.Domain.Enums;
using System;
using System.Collections.Generic;
using System.Security.Cryptography.X509Certificates;
using System.Text;

namespace Persons.Core.Commands.CreatePerson
{
    public class CreatePersonCommandValidator : AbstractValidator<CreatePersonCommand>
    {
        public CreatePersonCommandValidator(IStringLocalizer<ValidationResource> localizer)
        {
            RuleFor(x => x.FirstName)
                .Matches("^([a-zA-Z]{2,50}|[ა-ჰ]{2,50})$")
                .WithMessage(localizer["FirstNameNotValid"]);

            RuleFor(x => x.LastName)
                .Matches("^([a-zA-Z]{2,50}|[ა-ჰ]{2,50})$")
                .WithMessage(localizer["LastNameNotValid"]);

            RuleFor(x => x.PersonalNumber)
                .Matches("^\\d{11}$")
                .WithMessage(localizer["PersonalNumberNotValid"]);

            RuleFor(x => x.BirthDate)
                .ExclusiveBetween(DateTime.MinValue, DateTime.Now.AddYears(-18))
                .WithMessage(localizer["BirthDateNotValid"]);

            RuleFor(x => x.GenderId)
                            .Must(x => Enum.IsDefined(typeof(GenderEnum), x))
                            .WithMessage("GenderNotValid");

            RuleForEach(x => x.Mobiles).ChildRules(orders =>
            {
                orders.RuleFor(x => x.Number)
                .Matches("^\\d{4,50}$")
                .WithMessage("MobileNumberNotValid");

                orders.RuleFor(x => x.MobileTypeId)
                .Must(x => Enum.IsDefined(typeof(MobileTypeEnum), x))
                .WithMessage("MobileTypeNotValid");

            });

        }


    }
}
