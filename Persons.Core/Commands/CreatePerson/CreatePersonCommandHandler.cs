﻿using AutoMapper;
using MediatR;
using Persons.Core.Domain;
using Persons.Core.Common.Exceptions;
using Persons.Core.Repositories;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Localization;
using Persons.Core.Common.Resources;

namespace Persons.Core.Commands.CreatePerson
{
    public class CreatePersonCommandHandler : IRequestHandler<CreatePersonCommand>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public CreatePersonCommandHandler(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public async Task<Unit> Handle(CreatePersonCommand request, CancellationToken cancellationToken)
        {
            var person = _mapper.Map<CreatePersonCommand, Person>(request);

            if (person is null)
                throw new ArgumentNullException("Person");

            await _unitOfWork.PersonRepository.AddAsync(person);

            await _unitOfWork.CompleteAsync();

            return Unit.Value;
        }
    }

}
