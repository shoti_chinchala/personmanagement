﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace Persons.Core.Commands.CreatePerson
{
    public class CreatePersonCommand : IRequest
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string PersonalNumber { get; set; }
        public DateTime BirthDate { get; set; }
        public int GenderId { get; set; }
        public int CityId { get; set; }
        public IEnumerable<CreateMobileDTO> Mobiles { get; set; }
    }

    public class CreateMobileDTO
    {
        public int MobileTypeId { get; set; }
        public string Number { get; set; }
    }
}
