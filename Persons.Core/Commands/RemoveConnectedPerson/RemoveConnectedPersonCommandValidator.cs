﻿using FluentValidation;
using Microsoft.Extensions.Localization;
using Persons.Core.Common.Resources;
using System;
using System.Collections.Generic;
using System.Text;

namespace Persons.Core.Commands.RemoveConnectedPerson
{
    public class RemoveConnectedPersonCommandValidator : AbstractValidator<RemoveConnectedPersonCommand>
    {
        public RemoveConnectedPersonCommandValidator(IStringLocalizer<ValidationResource> localizer)
        {
            RuleFor(x => x.PersonId)
               .GreaterThan(0)
               .WithMessage(localizer["IdShouldBeMoreThanZero"]);

            RuleFor(x => x.ConnectedPersonId)
                .GreaterThan(0)
                .WithMessage(localizer["IdShouldBeMoreThanZero"]);

        }
    }
}
