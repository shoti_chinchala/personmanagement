﻿using MediatR;
using Microsoft.Extensions.Localization;
using Persons.Core.Common.Exceptions;
using Persons.Core.Common.Resources;
using Persons.Core.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Persons.Core.Commands.RemoveConnectedPerson
{
    public class RemoveConnectedPersonCommandHandler : IRequestHandler<RemoveConnectedPersonCommand>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IStringLocalizer<ValidationResource> _localizer;

        public RemoveConnectedPersonCommandHandler(IUnitOfWork unitOfWork, IStringLocalizer<ValidationResource> localizer)
        {
            _unitOfWork = unitOfWork;
            _localizer = localizer;
        }

        public async Task<Unit> Handle(RemoveConnectedPersonCommand request, CancellationToken cancellationToken)
        {
            var person = await _unitOfWork.PersonRepository.GetAsync(request.PersonId);
            if (person is null)
                throw new NotFoundException(_localizer["EntityNotFound", "Person", request.PersonId]);

            var connection = person.Connections.FirstOrDefault(x => x.PersonId == request.PersonId && x.ConnectedPersonId == request.ConnectedPersonId);
            if (connection is null)
                throw new NotFoundException(_localizer["EntityNotFound", "Connection", request.ConnectedPersonId]);

            person.Connections.ToList().Remove(connection);

            await _unitOfWork.CompleteAsync();

            return Unit.Value;
        }
    }
}
