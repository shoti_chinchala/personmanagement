﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace Persons.Core.Commands.RemoveConnectedPerson
{
    public class RemoveConnectedPersonCommand : IRequest
    {
        public int PersonId { get; set; }
        public int ConnectedPersonId { get; set; }
    }
}
