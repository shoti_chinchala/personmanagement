﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Persons.Core.Commands.AddConnectedPerson;
using Persons.Core.Commands.CreatePerson;
using Persons.Core.Commands.DeletePerson;
using Persons.Core.Commands.RemoveConnectedPerson;
using Persons.Core.Commands.UpdatePerson;
using Persons.Core.Commands.UploadImage;
using Persons.Core.Common.Models;
using Persons.Core.Queries.ConnectionsReport;
using Persons.Core.Queries.GetPerson;
using Persons.Core.Queries.GetPersonList;

namespace Persons.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PersonsController : BaseController
    {
        [HttpPost]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(ErrorResponse), (int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> Create(CreatePersonCommand request)
            => Ok(await Mediator.Send(request));

        [HttpPut("{id}")]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(ErrorResponse), (int)HttpStatusCode.BadRequest)]
        [ProducesResponseType(typeof(ErrorResponse), (int)HttpStatusCode.NotFound)]
        public async Task<IActionResult> Update(int id, [FromBody] UpdatePersonCommand request)
        {
            if (id != request.Id)
                return BadRequest();

            return Ok(await Mediator.Send(request));
        }

        [HttpGet("{id}")]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(ErrorResponse), (int)HttpStatusCode.NotFound)]
        public async Task<ActionResult<PersonDTO>> Get(int id)
            => Ok(await Mediator.Send(new GetPersonQuery { Id = id }));

        [HttpGet]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(ErrorResponse), (int)HttpStatusCode.BadRequest)]
        public async Task<ActionResult<List<PersonListDTO>>> GetList([FromQuery] GetPersonListQuery request)
             => Ok(await Mediator.Send(request));

        [HttpDelete("{id}")]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(ErrorResponse), (int)HttpStatusCode.BadRequest)]
        [ProducesResponseType(typeof(ErrorResponse), (int)HttpStatusCode.NotFound)]
        public async Task<IActionResult> Delete(int id)
            => Ok(await Mediator.Send(new DeletePersonCommand { Id = id }));

        [HttpPut("{id}/uploadImage")]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(ErrorResponse), (int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> UploadImage(int id, IFormFile file)
        {
            return Ok(await Mediator.Send(new UploadImageCommand { Id = id, File = file }));
        }

        [HttpPost("{id}/addConnectedPerson")]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(ErrorResponse), (int)HttpStatusCode.BadRequest)]
        [ProducesResponseType(typeof(ErrorResponse), (int)HttpStatusCode.NotFound)]
        public async Task<IActionResult> AddConnectedPerson(int id, AddConnectedPersonCommand request)
        {
            if (id != request.PersonId)
                return BadRequest();

            return Ok(await Mediator.Send(request));
        }

        [HttpDelete("{id}/removeConnectedPerson")]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(ErrorResponse), (int)HttpStatusCode.BadRequest)]
        [ProducesResponseType(typeof(ErrorResponse), (int)HttpStatusCode.NotFound)]
        public async Task<IActionResult> RemoveConnectedPerson(int id, RemoveConnectedPersonCommand request)
        {
            if (id != request.PersonId)
                return BadRequest();

            return Ok(await Mediator.Send(request));
        }

        [HttpGet("connectionsReport")]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        public async Task<ActionResult<List<ConnectionsReporDTO>>> GetConnectionsReport()
            => Ok(await Mediator.Send(new ConnectionsReportQuery { }));
    }
}