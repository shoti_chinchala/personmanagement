﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Localization;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;

namespace Persons.Api.Configurations
{
    public static class LocalizationConfiguration
    {
        private static readonly CultureInfo[] _supportedCultures = new[]
         {
                new CultureInfo("ka"),
                new CultureInfo("en")
            };

        public static IApplicationBuilder UseLocalization(this IApplicationBuilder app)
        {
            return app.UseRequestLocalization(new RequestLocalizationOptions()
            {
                DefaultRequestCulture = new RequestCulture("ka"),
                SupportedCultures = _supportedCultures,
                SupportedUICultures = _supportedCultures
            });
        }
    }
}
