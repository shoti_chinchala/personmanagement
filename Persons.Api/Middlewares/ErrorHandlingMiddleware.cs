﻿using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Persons.Core.Common.Exceptions;
using Persons.Core.Common.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace Persons.Api.Middlewares
{
    public class ErrorHandlingMiddleware
    {
        private readonly RequestDelegate _next;
        private readonly ILogger _logger;


        public ErrorHandlingMiddleware(RequestDelegate next, ILogger<ErrorHandlingMiddleware> logger)
        {
            _next = next;
            _logger = logger;
        }

        public async Task Invoke(HttpContext context)
        {
            try
            {
                await _next(context);
            }
            catch (ValidationException ex)
            {
                await catchError(HttpStatusCode.BadRequest, ex.Message, ex.Errors);
            }
            catch (NotFoundException ex)
            {
                await catchError(HttpStatusCode.BadRequest, ex.Message);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Unhandled Error");
                await catchError(HttpStatusCode.InternalServerError, ex.Message);
            }

            async Task catchError(HttpStatusCode statusCode, string message, List<string> errors = null)
            {
                context.Response.StatusCode = (int)statusCode;
                context.Response.ContentType = "application/json; charset=utf-8";
                await context.Response.WriteAsync(JsonConvert.SerializeObject(new ErrorResponse { Message = message, ValidationErrors = errors }));
            }
        }
    }
}
