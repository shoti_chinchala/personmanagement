﻿using Microsoft.EntityFrameworkCore;
using Persons.Core.Common.Models;
using Persons.Core.Domain;
using Persons.Core.Domain.Enums;
using Persons.Core.Queries.ConnectionsReport;
using Persons.Core.Queries.GetPersonList;
using Persons.Core.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace Persons.Infrastructure.Persistence.Repositories
{
    public class PersonRepository : Repository<Person>, IPersonRepository
    {
        private PersonContext personContext => Context as PersonContext;

        public PersonRepository(PersonContext context)
            : base(context)
        { }

        public override async Task<Person> GetAsync(int id)
        {
            var person = personContext
                .Persons
                .Include(x=>x.City)
                .Include(x=>x.Gender)
                .Include(x => x.Connections)
                    .ThenInclude(x => x.ConnectedPerson)
                .Include(x => x.Mobiles)
                    .ThenInclude(x => x.MobileType)
                .Where(x => x.Id == id);

            return await person.FirstOrDefaultAsync();
        }

        public async Task<IEnumerable<Person>> ListAsync(PersonFilter filter)
        {
            var persons = personContext.Persons.AsNoTracking();

            var filtered = persons;
            if (!string.IsNullOrEmpty(filter.FirstName))
                filtered = filtered.Where(x => EF.Functions.Like(x.FirstName, "%" + filter.FirstName + "%"));

            if (!string.IsNullOrEmpty(filter.LastName))
                filtered = filtered.Where(x => EF.Functions.Like(x.LastName, "%" + filter.LastName + "%"));

            if (!string.IsNullOrEmpty(filter.PersonalNumber))
                filtered = filtered.Where(x => EF.Functions.Like(x.PersonalNumber, "%" + filter.PersonalNumber + "%"));

            if (filter.GenderId != null)
                filtered = filtered.Where(x => x.GenderId == filter.GenderId);

            if (filter.BirthDate != null)
                filtered = filtered.Where(x => x.BirthDate == filter.BirthDate);

            if (filter.CityId != null)
                filtered = filtered.Where(x => x.CityId == filter.CityId);

            if (!string.IsNullOrEmpty(filter.MobileNumber))
                filtered = filtered.Where(x => x.Mobiles.Any(x => x.Number == filter.MobileNumber));

            var cnt = await filtered.CountAsync();

            var perPage = (int)filter.PerPage;
            var page = (filter.Page ?? 0) - 1;
            page = page > 0 ? page : 0;

            return await filtered.Skip(page * perPage).Take(perPage).ToListAsync();
        }

        public async Task<IEnumerable<ConnectionsReporDTO>> ConnectionsReport()
        {
            var persons = await personContext
                .Connections
                .Include(x => x.Person)
                .GroupBy(x => new { x.PersonId, x.ConnectionTypeId, x.Person.FirstName, x.Person.LastName, x.Person.PersonalNumber })
                .Select(x => new ConnectionsReporDTO
                {
                    Id = x.Key.PersonId,
                    FirstName = x.Key.FirstName,
                    LastName = x.Key.LastName,
                    PersonalNumber = x.Key.PersonalNumber,
                    ConnectionTypeId = (int)x.Key.ConnectionTypeId,
                    Connections = x.Count(),
                }).ToListAsync();

            return persons;
        }
    }
}
