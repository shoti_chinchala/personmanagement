﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Persons.Core.Common.Extensions;
using Persons.Core.Domain;
using Persons.Core.Domain.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Persons.Infrastructure.Persistence.Configurations
{
    public static class DataSeeding
    {
        public static void SeedData(this ModelBuilder builder)
        {
            builder.Entity<Gender>()
                .HasData(
                 Enum.GetValues(typeof(GenderEnum))
                    .Cast<GenderEnum>()
                    .Select(e => new Gender()
                    {
                        Id = e,
                        Name = e.GetDisplayName()
                    })
                );

            builder.Entity<MobileType>()
               .HasData(
                Enum.GetValues(typeof(MobileTypeEnum))
                   .Cast<MobileTypeEnum>()
                   .Select(e => new MobileType()
                   {
                       Id = e,
                       Name = e.GetDisplayName()
                   })
               );

            builder.Entity<ConnectionType>()
              .HasData(
               Enum.GetValues(typeof(ConnectionTypeEnum))
                  .Cast<ConnectionTypeEnum>()
                  .Select(e => new ConnectionType()
                  {
                      Id = e,
                      Name = e.GetDisplayName()
                  })
              );


            builder.Entity<City>().
               HasData(
                   new City { Id = 1, Name = "თბილისი" },
                   new City { Id = 2, Name = "ბათუმი" },
                   new City { Id = 3, Name = "ქუთაისი" }
             );
        }
    }
}
