﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Persons.Core.Domain;
using System;
using System.Collections.Generic;
using System.Text;

namespace Persons.Infrastructure.Persistence.Configurations
{
    class MobileConfiguration : IEntityTypeConfiguration<Mobile>
    {
        public void Configure(EntityTypeBuilder<Mobile> builder)
        {
            builder
                .HasKey(x => x.Id);

            builder
                .Property(x => x.Id)
                .UseIdentityColumn();

            builder
                .Property(x => x.Number)
                .HasMaxLength(50);

            builder
                .Property(x => x.MobileTypeId)
                .HasConversion<int>();
        }
    }
}
