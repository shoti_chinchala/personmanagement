﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Persons.Core.Domain;
using System;
using System.Collections.Generic;
using System.Text;

namespace Persons.Infrastructure.Persistence.Configurations
{
    public class ConnectionConfiguration : IEntityTypeConfiguration<Connection>
    {
        public void Configure(EntityTypeBuilder<Connection> builder)
        {
            builder
                .HasKey(x => new { x.PersonId, x.ConnectedPersonId });

            builder
                .HasOne(x => x.ConnectedPerson)
                .WithMany()
                .HasForeignKey(x => x.ConnectedPersonId)
                .OnDelete(DeleteBehavior.NoAction);

            builder
                .HasOne(x => x.Person)
                .WithMany(x => x.Connections)
                .HasForeignKey(x => x.PersonId);

            builder
                .Property(x => x.ConnectionTypeId)
                .HasConversion<int>();
        }
    }
}
