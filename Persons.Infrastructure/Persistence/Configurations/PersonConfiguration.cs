﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Persons.Core.Domain;
using System;
using System.Collections.Generic;
using System.Text;

namespace Persons.Infrastructure.Persistence.Configurations
{
    public class PersonConfiguration : IEntityTypeConfiguration<Person>
    {
        public void Configure(EntityTypeBuilder<Person> builder)
        {
            builder
                .HasKey(x => x.Id);

            builder.HasOne(x => x.Gender)
                .WithMany()
                .HasForeignKey(x => (int)x.GenderId);

            builder
                .Property(x => x.Id)
                .UseIdentityColumn();

            builder
                .Property(x => x.FirstName)
                .IsRequired()
                .HasMaxLength(50);

            builder
                .Property(x => x.LastName)
                .IsRequired()
                .HasMaxLength(50);

            builder
                .Property(x => x.PersonalNumber)
                .IsRequired()
                .HasMaxLength(11);

            builder
                .Property(x => x.BirthDate)
                .IsRequired()
                .HasColumnType("Date");

            builder
                .HasMany(x => x.Mobiles)
                .WithOne();

            builder
                .Property(x => x.GenderId)
                .HasConversion<int>();

            builder
                .HasQueryFilter(x => !x.IsDeleted);
        }
    }
}
