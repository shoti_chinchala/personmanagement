﻿using Persons.Core.Domain;
using Persons.Core.Repositories;
using Persons.Infrastructure.Persistence.Repositories;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Persons.Infrastructure.Persistence
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly PersonContext _context;

        public UnitOfWork(PersonContext context)
        {
            _context = context ?? throw new ArgumentNullException(nameof(context));
            PersonRepository = new PersonRepository(context);
        }

        public IPersonRepository PersonRepository { get; private set; }

        public async Task<int> CompleteAsync()
        {
            return await _context.SaveChangesAsync();
        }

        public void Dispose()
        {
            _context.Dispose();
        }

    }
}
