﻿using Microsoft.EntityFrameworkCore;
using Persons.Core.Domain;
using Persons.Infrastructure.Persistence.Configurations;
using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Persons.Infrastructure.Persistence
{
    public class PersonContext : DbContext
    {
        public DbSet<Person> Persons { get; set; }
        public DbSet<Mobile> Mobiles{ get; set; }
        public DbSet<City> Cities{ get; set; }
        public DbSet<Gender> Genders{ get; set; }
        public DbSet<MobileType> MobileTypes{ get; set; }
        public DbSet<Connection> Connections { get; set; }
        public DbSet<ConnectionType> ConnectionTypes { get; set; }

        public PersonContext(DbContextOptions<PersonContext> options) : base(options)
        {

        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.ApplyConfigurationsFromAssembly(Assembly.GetExecutingAssembly());
            builder.SeedData();
            base.OnModelCreating(builder);
        }

        public override int SaveChanges()
        {
            UpdateDeleteStatuses();
            return base.SaveChanges();
        }

        public override Task<int> SaveChangesAsync(CancellationToken cancellationToken = default)
        {
            UpdateDeleteStatuses();
            return base.SaveChangesAsync(cancellationToken);
        }

        private void UpdateDeleteStatuses()
        {
            foreach (var entry in ChangeTracker.Entries())
            {
                if (entry.State == EntityState.Deleted && entry.Entity is IIsDeleted)
                {
                    entry.State = EntityState.Modified;
                    ((IIsDeleted)entry.Entity).IsDeleted = true;
                }
            }
        }
    }
}
