﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Persons.Core.Repositories;
using Persons.Infrastructure.Persistence;
using System;
using System.Collections.Generic;
using System.Text;

namespace Persons.Infrastructure
{
    public static class DependencyInjection
    {
        public static IServiceCollection AddInfrastructure(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddDbContext<PersonContext>(option => option.UseSqlServer(configuration.GetConnectionString("Person")));
            services.AddTransient<IUnitOfWork, UnitOfWork>();
            return services;
        }
    }
}
